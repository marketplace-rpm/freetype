# Information / Информация

SPEC-файл для создания RPM-пакета **freetype**.

## Install / Установка

1. Подключить репозиторий **MARKETPLACE**: `dnf copr enable marketplace/freetype`.
2. Установить пакет: `dnf install freetype`.

## Настройка / Configuration

В файл `/etc/fonts/local.conf` добавить:

```xml
<?xml version='1.0'?>
<!DOCTYPE fontconfig SYSTEM 'fonts.dtd'>
<fontconfig>
  <match target="font">
    <edit mode="assign" name="rgba">
    <const>rgb</const>
    </edit>
  </match>
  <match target="font">
    <edit mode="assign" name="hinting">
    <bool>true</bool>
    </edit>
  </match>
  <match target="font">
    <edit mode="assign" name="hintstyle">
    <const>hintfull</const>
    </edit>
  </match>
  <match target="font">
    <edit mode="assign" name="antialias">
    <bool>true</bool>
    </edit>
  </match>
  <match target="font">
    <edit mode="assign" name="lcdfilter">
    <const>lcddefault</const>
    </edit>
  </match>
</fontconfig>
```

## Donation / Пожертвование

- [Donating](https://donating.gitlab.io/)